package br.com.serializandows.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Classe entidade item serie
 * @author Pedro Jatoba
 *
 */
@XmlRootElement
public class ItemSerie {
	
	private Integer id_item_serie;
	private String nome_serie;
	private String link_imagem;
	private String descricao_serie_completa;
	private String descricao_serie_simples;
	private int temporadas;
	
	public Integer getId_item_serie() {
		return id_item_serie;
	}
	public void setId_item_serie(Integer id_item_serie) {
		this.id_item_serie = id_item_serie;
	}
	public String getNome_serie() {
		return nome_serie;
	}
	public void setNome_serie(String nome_serie) {
		this.nome_serie = nome_serie;
	}
	public String getLink_imagem() {
		return link_imagem;
	}
	public void setLink_imagem(String link_imagem) {
		this.link_imagem = link_imagem;
	}
	public String getDescricao_serie_completa() {
		return descricao_serie_completa;
	}
	public void setDescricao_serie_completa(String descricao_serie_completa) {
		this.descricao_serie_completa = descricao_serie_completa;
	}
	public String getDescricao_serie_simples() {
		return descricao_serie_simples;
	}
	public void setDescricao_serie_simples(String descricao_serie_simples) {
		this.descricao_serie_simples = descricao_serie_simples;
	}
	public int getTemporadas() {
		return temporadas;
	}
	public void setTemporadas(int temporadas) {
		this.temporadas = temporadas;
	}
	@Override
	public String toString() {
		return "item_serie [id_item_serie=" + id_item_serie + ", nome_serie=" + nome_serie + ", link_imagem="
				+ link_imagem + ", descricao_serie_completa=" + descricao_serie_completa + ", descricao_serie_simples="
				+ descricao_serie_simples + ", temporadas=" + temporadas + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao_serie_completa == null) ? 0 : descricao_serie_completa.hashCode());
		result = prime * result + ((descricao_serie_simples == null) ? 0 : descricao_serie_simples.hashCode());
		result = prime * result + ((id_item_serie == null) ? 0 : id_item_serie.hashCode());
		result = prime * result + ((link_imagem == null) ? 0 : link_imagem.hashCode());
		result = prime * result + ((nome_serie == null) ? 0 : nome_serie.hashCode());
		result = prime * result + temporadas;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemSerie other = (ItemSerie) obj;
		if (descricao_serie_completa == null) {
			if (other.descricao_serie_completa != null)
				return false;
		} else if (!descricao_serie_completa.equals(other.descricao_serie_completa))
			return false;
		if (descricao_serie_simples == null) {
			if (other.descricao_serie_simples != null)
				return false;
		} else if (!descricao_serie_simples.equals(other.descricao_serie_simples))
			return false;
		if (id_item_serie == null) {
			if (other.id_item_serie != null)
				return false;
		} else if (!id_item_serie.equals(other.id_item_serie))
			return false;
		if (link_imagem == null) {
			if (other.link_imagem != null)
				return false;
		} else if (!link_imagem.equals(other.link_imagem))
			return false;
		if (nome_serie == null) {
			if (other.nome_serie != null)
				return false;
		} else if (!nome_serie.equals(other.nome_serie))
			return false;
		if (temporadas != other.temporadas)
			return false;
		return true;
	}
	
	
}
