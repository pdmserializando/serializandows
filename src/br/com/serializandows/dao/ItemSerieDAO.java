package br.com.serializandows.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import br.com.serializandows.factory.FabricaConexao;
import br.com.serializandows.model.ItemSerie;

/**
 * Classe de persistencia
 * @author Pedro Jatoba
 *
 */
public class ItemSerieDAO extends FabricaConexao{

	/**
	 * Intancia unica da classe DAO (Singleton)
	 */
	private static ItemSerieDAO instancia;
	
	/**
	 * Retorna a instancia unica do DAO, e caso n�o tenha sido iniciada se inicia
	 * @return
	 */
	public static ItemSerieDAO getInstancia() {
		if(instancia == null){
			instancia = new ItemSerieDAO();
		}
		return instancia;
	}

	/**
	 * Metodo para pesquisar todos os itens serie;
	 * @return
	 */
	public ArrayList<ItemSerie> listarItens(){
		Connection conexao = null;
		ArrayList<ItemSerie> todosItens = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		conexao = criarConexao();
		
		try {
			pstmt = conexao.prepareStatement("SELECT * FROM item_serie ORDER BY nome_serie");
			rs = pstmt.executeQuery();
			while(rs.next()){
				ItemSerie item = new ItemSerie();
				
				item.setId_item_serie(rs.getInt("id_item_serie"));
				item.setNome_serie(rs.getString("nome_serie"));
				item.setLink_imagem(rs.getString("link_imagem"));
				item.setDescricao_serie_completa(rs.getString("descricao_serie_completa"));
				item.setDescricao_serie_simples(rs.getString("descricao_serie_simples"));
				item.setTemporadas(rs.getInt("temporadas"));
				
				todosItens.add(item);
			}
			
		} catch (Exception e) {
			System.out.println("Erro ao buscar todos os itens");
		} finally {
			fecharConexao(conexao, pstmt, rs);
		}
		
		return todosItens;
	}
	
}
