package br.com.serializandows.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.serializandows.factory.FabricaConexao;
import br.com.serializandows.model.ItemSerie;
import br.com.serializandows.model.Usuario;

public class UsuarioDAO extends FabricaConexao {
	

	public UsuarioDAO() {

	}

	/**
	 * Intancia unica da classe DAO (Singleton)
	 */
	private static UsuarioDAO instancia;

	/**
	 * Retorna a instancia unica do DAO, e caso n�o tenha sido iniciada se
	 * inicia
	 * 
	 * @return
	 */
	public static UsuarioDAO getInstancia() {
		if (instancia == null) {
			instancia = new UsuarioDAO();
		}
		return instancia;
	}

	public boolean incluirUsuario(Usuario us) {
		Connection conexao = null;
		String sqlInsert = "INSERT INTO usuario (nome_usuario, senha) VALUES ('?', '?')";
		PreparedStatement stm = null;
		conexao = criarConexao();
		try {
			stm = conexao.prepareStatement(sqlInsert);
			stm.setString(1, us.getNome_usuario());
			stm.setString(2, us.getSenha());
			stm.execute();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				System.out.print(e1.getStackTrace());
			}
			return false;
			
		} finally {
			if (stm != null) {
				try {
					stm.close();
				} catch (SQLException e1) {
					System.out.print(e1.getStackTrace());
				}
			}
		}
		return true;
	}

	public Usuario buscarUsuario(String login,String senha) {
		Connection conexao = null;
		String sqlSelect = "SELECT id_usuario, nome_usuario, nick_name,email, senha FROM usuario WHERE usuario.nome_usuario = ? AND usuario.senha = ?";
		PreparedStatement stm = null;
		ResultSet rs = null;
		conexao = criarConexao();
		Usuario us = new Usuario();
		try {
			stm = conexao.prepareStatement(sqlSelect);
			stm.setString(0, login);
			stm.setString(1, senha);
			rs = stm.executeQuery();
			if (rs.next()) {
				us.setId_usuario(rs.getInt(1));
				us.setNome_usuario(rs.getString(2));
				us.setNick_name(rs.getString(3));
				us.setEmail(rs.getString(3));
				us.setSenha(rs.getString(5));
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				System.out.print(e1.getStackTrace());
			}
		} finally {
			if (stm != null) {
				try {
					stm.close();
				} catch (SQLException e1) {
					System.out.print(e1.getStackTrace());
				}
			}
		}
		return us;
	}

}