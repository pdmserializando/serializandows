package br.com.serializandows.resource;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.serializandows.controller.ControladorItemSerie;
import br.com.serializandows.model.ItemSerie;

/**
 * Classe detentora da inteligencia de conexao e utilização dos metodos de WS 
 * @author Pedro Jatoba
 *
 */
@Path("/itemSerie")
public class RecursosItemSerie {

	/**
	 * Metodo responsavel por fazer chamada ao controller
	 * @return
	 */
	@GET
	@Path("/listarTodasSeries")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<ItemSerie> listarSeries(){
		return new ControladorItemSerie().listarTodosItens();		
	}
}
