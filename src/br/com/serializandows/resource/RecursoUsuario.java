package br.com.serializandows.resource;


import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.serializandows.controller.ControladorUsuario;
import br.com.serializandows.model.Usuario;

/**
 * Classe detentora da inteligencia de conexao e utilização dos metodos de WS 
 * @author Pedro Jatoba
 *
 */
@Path("/usuario")
public class RecursoUsuario {
	String login, senha;
	
	
	/**
	 * Metodo responsavel por fazer chamada ao controller
	 * @return
	 */
	@GET
	@Path("/{login}/{senha}")
	@Produces(MediaType.APPLICATION_JSON)
	public Usuario fazLogin(@FormParam("login") String login,
			@FormParam("senha") String senha){
		return new ControladorUsuario().fazLogin(login, senha);		
	}
	
	/**
	 * Metodo responsavel por fazer chamada ao controller
	 * @return
	 */
	@GET
	@Path("/criaUsuario/{login}/{senha}")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean adicionarUsuario(@FormParam("login") String login,
			@FormParam("senha") String senha){
		System.out.println(login + senha);
		return new ControladorUsuario().adicionaUsuario(login, senha);		
	}
}


