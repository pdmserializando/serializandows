package br.com.serializandows.controller;

import java.util.ArrayList;

import br.com.serializandows.dao.ItemSerieDAO;
import br.com.serializandows.model.ItemSerie;

public class ControladorItemSerie {
	
	public ArrayList<ItemSerie> listarTodosItens(){
		return ItemSerieDAO.getInstancia().listarItens();
	}
	
}
