package br.com.serializandows.controller;


import java.util.ArrayList;

import br.com.serializandows.dao.UsuarioDAO;
import br.com.serializandows.model.Usuario;

public class ControladorUsuario {

	Usuario us = new Usuario();
	
	public Usuario fazLogin(String login,String senha){
		return UsuarioDAO.getInstancia().buscarUsuario(login, senha);
	}
	
	public boolean adicionaUsuario(String login,String senha){
		Usuario us = new Usuario(login, senha);
		return UsuarioDAO.getInstancia().incluirUsuario(us);
	}
}